import java.util.ArrayList;
import java.util.List;


/**
 * This creates the buffer and the producer and consumer threads.
 *
 */
public class Factory
{
	public static void main(String args[]) {
		if (args.length < 3) {
			System.out.println("Invalid number of command line arguments, (Expecting three integer values)");
			System.exit(1);
		}
		int sleep_time = Integer.parseInt(args[0]);
		int num_producers = Integer.parseInt(args[1]);
		int num_consumers = Integer.parseInt(args[2]);
		Buffer server = new BoundedBuffer();
		List<Thread> producerList = new ArrayList<Thread>();
		List<Thread> consumerList = new ArrayList<Thread>();
      		// now create the producer and consumer threads
			int counter = 0;
			while(counter < num_producers) {
      			producerList.add(new Thread(new Producer(server)));
			    counter++;
			}
			counter = 0;
			while (counter < num_consumers) {
				consumerList.add(new Thread(new Consumer(server)));
				counter++;
			}
      		for (Thread thread : producerList) {
				thread.start();
			}
      		for (Thread thread : consumerList) {
				thread.start();
			}  
          	     long t0, t1;
                 t0 =  System.currentTimeMillis();
                 do{
                     t1 = System.currentTimeMillis();
                 }
                 while ((t1 - t0) < (sleep_time * 1000));
            System.out.println("\n----------------Done Sleeping, Goodbye---------------");
      	    System.exit(0);
	}
}
