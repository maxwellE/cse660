#!/usr/bin/env ruby
include Process
history = []
counter = 1
if File.exists?("#{Process.uid}.history") and File.readable?("#{Process.uid}.history")
  in_file = File.open("#{Process.uid}.history")
  counter = in_file.gets.strip.to_i 
  in_file.each_line{|line| history.push line.strip}
  in_file.close
end
$stdout.sync
  Signal.trap("INT") do
    index = (counter-10)
    if index < 1
      index = 1 
    end
    puts ""
    history.each do |n|
      puts "#{index}: #{n}"
      index +=1
    end
    print "COMMAND->"
  end
while true do
  print "COMMAND->"
  input = gets
  if input.nil?
    puts "\nSaving command history in #{Process.uid}.history......"
    history_file = File.new("#{Process.uid}.history", "w+")
    history_file.puts counter.to_s
    history.each do |cmd|
      history_file.puts cmd
    end
    history_file.close
    exit(0)
  end
  if input.start_with? "r"
    if input.length == 2
      if history.last
        input = history.last
      end
     elsif input.length == 3 
    results = history.select{|x| x.start_with? input[1]}
    unless results.size <1
      input = results.last
    end
    end
  end
  if history.length > 9
    history.shift
    history.push(input)
    counter +=1
  else
    history.push(input)
    counter +=1
  end
  pid = fork do
    begin
    exec input
    kill(pid)
    rescue SystemCallError,ArgumentError
    end
  end
  unless pid == 0
    Process.wait(pid)
  end
end

