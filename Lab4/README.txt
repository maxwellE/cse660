Lab 4
============================= 
CSE 660 Mathis

By: Maxwell Elliott

Installation
---------------------
	You will need a Ruby interpreter to run this code.  All CSE computers in Caldwell lab have an aged ruby interpreter on them, it should will work for this file.  If you are running your computer on your local machine you can check to see if you have ruby installed with the following command:

$ ruby -v

which should print out something like this(numbers will be different):

maxwell@maxwell-VirtualBox:~$ ruby -v
ruby 1.9.3p194 (2012-04-20 revision 35410) [x86_64-linux]

If this reads ruby 1.8.7 or higher you are golden, if not you will need to install a ruby interpreter from here: http://www.ruby-lang.org/en/downloads/ .  You should'nt have to come to this point since all Macs and Linux distros ship with ruby 1.8.7 or higher. This code does not work on windows, period.

Instructions
---------------------
	The Lab 4 shell operates in the same manner as the shells from labs 3 and 1.  To run the shell type the following
on the command prompt:

$ ./shell.rb

This will open the shell you know and love.  When you exit the shell your history will be saved in a .history file in
the current directory.  This file should not be tampered with, doing so will cause issues with the program.  You may
delete your history file to restart your history at any time.  
