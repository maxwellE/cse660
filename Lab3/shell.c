#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#define MAX_LINE 80 /* 80 chars per line, per command, should be enough. */

//  Lab2: Maxwell Elliott

#define BUFFER_SIZE 50
char buffer_ary[10][BUFFER_SIZE];
static char buffer[BUFFER_SIZE];
int counter = 0;
/* the signal handler function */
void handle_SIGINT() {
    int loop = 0;
    write(STDOUT_FILENO,"\n", 2);
    while(loop < 10) {
      char cmdstr[50];
      snprintf( cmdstr, 20, "%d: %s\n" , loop, buffer_ary[loop]);
      write(STDOUT_FILENO,cmdstr,strlen(cmdstr));
      loop++;
    }
    write(STDOUT_FILENO,"COMMAND->", 9);
}

/**
 * setup() reads in the next command line, separating it into distinct tokens
 * using whitespace as delimiters. setup() sets the args parameter as a 
 * null-terminated string.
 */

void setup(char inputBuffer[], char *args[],int *background)
{
    int length, /* # of characters in the command line */
        i,      /* loop index for accessing inputBuffer array */
        start,  /* index where beginning of next command parameter is */
        ct;     /* index of where to place the next parameter into args[] */
    
    ct = 0;
    
    /* read what the user enters on the command line */
    length = read(STDIN_FILENO, inputBuffer, MAX_LINE);  
    strcpy(buffer, inputBuffer);
    int count = 0;
    while(count < strlen(buffer))
    {
    buffer_ary[counter][count] = buffer[count];
    count++;
    }
    counter++;
    start = -1;
    if (length == 0)
        exit(0);            /* ^d was entered, end of user command stream */
    if (length < 0){
        perror("error reading the command");
	exit(-1);           /* terminate with error code of -1 */
    }
    
    /* examine every character in the inputBuffer */
    for (i = 0; i < length; i++) { 
        switch (inputBuffer[i]){
        case ' ':
        case '\t' :               /* argument separators */
            if(start != -1){
                args[ct] = &inputBuffer[start];    /* set up pointer */
                ct++;
            }
            inputBuffer[i] = '\0'; /* add a null char; make a C string */
            start = -1;
            break;
            
        case '\n':                 /* should be the final char examined */
            if (start != -1){
                args[ct] = &inputBuffer[start];     
                ct++;
            }
            inputBuffer[i] = '\0';
            args[ct] = NULL; /* no more arguments to this command */
            break;

        case '&':
            *background = 1;
            inputBuffer[i] = '\0';
            break;
            
        default :             /* some other character */
            if (start == -1)
                start = i;
	} 
    }    
    args[ct] = NULL; /* just in case the input line was > 80 */
} 

int main(void)
{
    char inputBuffer[MAX_LINE]; /* buffer to hold the command entered */
    int background;             /* equals 1 if a command is followed by '&' */
    char *args[MAX_LINE/2+1];/* command line (of 80) has max of 40 arguments */
    /* set up the signal handler */
    struct sigaction handler;
    handler.sa_handler = handle_SIGINT;
    sigaction(SIGINT, &handler, NULL);


    /* wait for <control> <C> */ 

    while (1){            /* Program terminates normally inside setup */
	    background = 0;
	      printf("COMMAND->");
        fflush(0);
        setup(inputBuffer, args, &background);       /* get next command */
	      pid_t pid;

      	/* the steps are:
	      (1) fork a child process using fork()
	      (2) the child process will invoke execvp()
	      (3) if background == 0, the parent will wait, 
		    otherwise returns to the setup() function. */
	      pid = fork();
	      if (pid == 0) { /* child process */
		    execvp(inputBuffer,args);
		    _exit(EXIT_FAILURE);
	      }
	      else if(pid == -1) {
	  	      perror("Fatal error encountered when forking process");
	      }
	    else { /* parent process */
		        /* parent will wait for the child to complete */
	        if(background == 0) {
	           waitpid(pid, &background,0);
	        }
	    }
    }
    return 0;
}
