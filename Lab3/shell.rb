#!/usr/bin/env ruby
include Process
history = [] 
counter = 0
$stdout.sync
  Signal.trap("INT") do
    index = (counter-10)
    if index < 0
      index = 1 
    end
    puts ""
    history.each do |n|
      puts "#{index}: #{n}"
      index +=1
    end
    print "COMMAND->"
  end
while true do
  print "COMMAND->"
  input = gets
  if input.nil?
    puts ""
    exit(0)
  end
  if input.start_with? "r"
    if input.length == 2
      if history.last
        input = history.last
      end
     elsif input.length == 3 
    results = history.select{|x| x.start_with? input[1]}
    unless results.size <1
      input = results.last
    end
    end
  end
  if history.length > 10
    history.shift
    history.push(input)
    counter +=1
  else
    history.push(input)
    counter +=1
  end
  pid = fork do
    begin
    exec input
    kill(pid)
    rescue SystemCallError,ArgumentError
    end
  end
  unless pid == 0
    Process.wait(pid)
  end
end

